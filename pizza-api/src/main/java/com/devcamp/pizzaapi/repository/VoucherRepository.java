package com.devcamp.pizzaapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizzaapi.models.Voucher;

public interface VoucherRepository extends JpaRepository<Voucher, Integer> {

}
