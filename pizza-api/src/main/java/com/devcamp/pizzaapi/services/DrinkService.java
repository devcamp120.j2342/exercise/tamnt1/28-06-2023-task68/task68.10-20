package com.devcamp.pizzaapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.devcamp.pizzaapi.models.Drink;
import com.devcamp.pizzaapi.repository.DrinkRepository;

@Service
public class DrinkService {
    private final DrinkRepository drinkRepository;

    public DrinkService(DrinkRepository drinkRepository) {
        this.drinkRepository = drinkRepository;
    }

    public List<Drink> getDrinkList() {
        return drinkRepository.findAll();
    }

    public Drink getDrinkById(Integer id) {
        Optional<Drink> optionalDrink = drinkRepository.findById(id);
        return optionalDrink.orElse(null);
    }

    public Drink createDrink(Drink drink) {
        return drinkRepository.save(drink);
    }

    public Drink updateDrink(Integer id, Drink updatedDrink) {
        Optional<Drink> optionalDrink = drinkRepository.findById(id);
        if (optionalDrink.isPresent()) {
            Drink existingDrink = optionalDrink.get();
            existingDrink.setMaNuocUong(updatedDrink.getMaNuocUong());
            existingDrink.setTenNuocUong(updatedDrink.getTenNuocUong());
            existingDrink.setDonGia(updatedDrink.getDonGia());
            existingDrink.setGhiChu(updatedDrink.getGhiChu());
            existingDrink.setNgayCapNhat(updatedDrink.getNgayCapNhat());
            return drinkRepository.save(existingDrink);
        } else {
            return null;
        }
    }

    public void deleteDrink(Integer id) {
        drinkRepository.deleteById(id);
    }
}
