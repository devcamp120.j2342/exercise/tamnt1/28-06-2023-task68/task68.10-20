package com.devcamp.pizzaapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.pizzaapi.models.Order;
import com.devcamp.pizzaapi.repository.OrderRepository;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Order> getOrders(String page, String size, String sortField, String sortDir) {
        int pageNumber = Integer.parseInt(page);
        int pageSize = Integer.parseInt(size);
        Sort sort;
        if (sortField.isEmpty()) {
            sort = Sort.by("id");
        } else {
            sort = Sort.by(sortField);
        }

        sort = sortDir.equals("dsc") ? sort.descending() : sort.ascending();

        Pageable pageableVoucher = PageRequest.of(pageNumber, pageSize, sort);
        Page<Order> orderPage = orderRepository.findAll(pageableVoucher);
        List<Order> orderList = orderPage.getContent();
        return orderList;
    }

    public Order getOrderById(Long id) {
        Optional<Order> optionalOrder = orderRepository.findById(id);
        return optionalOrder.orElse(null);
    }

    public Order getOrderByCustomerId(Long id) {
        return orderRepository.findByUser_Id(id);

    }

    public Order createOrder(Order order) {
        return orderRepository.save(order);
    }

    public Order updateOrder(Long id, Order updatedOrder) {
        Order order = getOrderById(id);
        order.setOrderCode(updatedOrder.getOrderCode());
        order.setKichCo(updatedOrder.getKichCo());
        order.setLoaiPizza(updatedOrder.getLoaiPizza());
        order.setThanhTien(updatedOrder.getThanhTien());
        order.setDrinks(updatedOrder.getDrinks());
        order.setUser(updatedOrder.getUser());
        order.setVoucherCode(updatedOrder.getVoucherCode());
        return orderRepository.save(order);
    }

    public void deleteOrder(Long id) {
        Order order = getOrderById(id);
        orderRepository.delete(order);
    }

    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }
}
