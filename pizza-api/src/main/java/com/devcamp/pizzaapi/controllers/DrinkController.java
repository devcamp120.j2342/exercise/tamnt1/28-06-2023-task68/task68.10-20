package com.devcamp.pizzaapi.controllers;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizzaapi.models.Drink;
import com.devcamp.pizzaapi.services.DrinkService;

@RestController
@CrossOrigin
@RequestMapping("/api/drinks")
public class DrinkController {
    private final DrinkService drinkService;

    public DrinkController(DrinkService drinkService) {
        this.drinkService = drinkService;
    }

    @GetMapping
    public List<Drink> getAllDrinks() {
        return drinkService.getDrinkList();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Drink> getDrinkById(@PathVariable Integer id) {
        Drink drink = drinkService.getDrinkById(id);
        if (drink != null) {
            return ResponseEntity.ok(drink);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<Drink> createDrink(@RequestBody Drink drink) {
        Drink createdDrink = drinkService.createDrink(drink);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdDrink);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Drink> updateDrink(@PathVariable Integer id, @RequestBody Drink drink) {
        Drink updatedDrink = drinkService.updateDrink(id, drink);
        if (updatedDrink != null) {
            return ResponseEntity.ok(updatedDrink);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDrink(@PathVariable Integer id) {
        drinkService.deleteDrink(id);
        return ResponseEntity.noContent().build();
    }
}
